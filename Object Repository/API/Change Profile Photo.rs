<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Change Profile Photo</name>
   <tag></tag>
   <elementGuidId>ece8c41b-d753-40c5-9f1f-d61e80b4b577</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNjJiZDAzM2Y4MDc1ZTJjZDY2MGZjNTU0ZDFkZmYwN2YzZDA5OTdhYTAzMDdiYzdiNWIzMjU4YjEyZGNhMDEzYjc0ZWJmNmMwMTZmNDZiOGYiLCJpYXQiOjE2ODkwNDYyNDUuMjYwMTIsIm5iZiI6MTY4OTA0NjI0NS4yNjAxMjUsImV4cCI6MTcyMDY2ODY0NS4yNTc4MDIsInN1YiI6IjU5OCIsInNjb3BlcyI6W119.W95LIsIsUMDI9V8TIL1lin8es6n_cEQdywEoZla2WUh5laXeMucUBeyde6k95_pVdZTijyOis5Gk0gRwvEeGHzk_ypjlHf9P8N-AOy5dtDYcm2FROlUGwpgLU0w8BIxHPMuZpiti7F3KjWqyMof3O15f0RfHlJsD7izFd9pwoaGuU0VxR4AT7p3Mljti2xvtRvoWYklxlJvHSO2e28-v2GXItLU3-xwckusWo1woYdIWDf-OSNL3S7xx6qA1PJ6PWOnl6pwxTLA9R4ZSY6IbmJ0bNZabqUmY_f_Ve59vQVacFkUUWBDDs2sZwHM1LLiCdF-4gbn9EFZTjShYqxnS-wH59toRdzrLc-5ONEKYU5UT6U_x-juWVZhW92UtUn-M_U3UxCaIuNZb-uruXCIXgSnhHoEawRJfiP45u03D8NEMYz9SSH1ACuhG3V8lOkHLtCSi8fKMyN-d3AM-lEtntv24sfEsQHwBApwyp5sAGzKt2N2RekHb0DppMCIXo-qe3DqqDTH8DC9-JPO6A8RCVa79ygNT4gqUa44_e13sNAnqeYlFzl-FLUB8QzUvhqYFVzl92_XY19WmP58z-XG3sz17H6llYhv5K6tRJaq2t0aUhQFF_QlgH2AITfJlT0qeqpDuI2I9L1plYtuy7AlNocOTzpoG5_twWEUCKsNxoDc</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;${photo}&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>cd776235-e6e3-4d9f-ab0f-ecfb7030f186</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNjJiZDAzM2Y4MDc1ZTJjZDY2MGZjNTU0ZDFkZmYwN2YzZDA5OTdhYTAzMDdiYzdiNWIzMjU4YjEyZGNhMDEzYjc0ZWJmNmMwMTZmNDZiOGYiLCJpYXQiOjE2ODkwNDYyNDUuMjYwMTIsIm5iZiI6MTY4OTA0NjI0NS4yNjAxMjUsImV4cCI6MTcyMDY2ODY0NS4yNTc4MDIsInN1YiI6IjU5OCIsInNjb3BlcyI6W119.W95LIsIsUMDI9V8TIL1lin8es6n_cEQdywEoZla2WUh5laXeMucUBeyde6k95_pVdZTijyOis5Gk0gRwvEeGHzk_ypjlHf9P8N-AOy5dtDYcm2FROlUGwpgLU0w8BIxHPMuZpiti7F3KjWqyMof3O15f0RfHlJsD7izFd9pwoaGuU0VxR4AT7p3Mljti2xvtRvoWYklxlJvHSO2e28-v2GXItLU3-xwckusWo1woYdIWDf-OSNL3S7xx6qA1PJ6PWOnl6pwxTLA9R4ZSY6IbmJ0bNZabqUmY_f_Ve59vQVacFkUUWBDDs2sZwHM1LLiCdF-4gbn9EFZTjShYqxnS-wH59toRdzrLc-5ONEKYU5UT6U_x-juWVZhW92UtUn-M_U3UxCaIuNZb-uruXCIXgSnhHoEawRJfiP45u03D8NEMYz9SSH1ACuhG3V8lOkHLtCSi8fKMyN-d3AM-lEtntv24sfEsQHwBApwyp5sAGzKt2N2RekHb0DppMCIXo-qe3DqqDTH8DC9-JPO6A8RCVa79ygNT4gqUa44_e13sNAnqeYlFzl-FLUB8QzUvhqYFVzl92_XY19WmP58z-XG3sz17H6llYhv5K6tRJaq2t0aUhQFF_QlgH2AITfJlT0qeqpDuI2I9L1plYtuy7AlNocOTzpoG5_twWEUCKsNxoDc</value>
      <webElementGuid>ea635581-62e3-4480-bc42-78ea9f25d1a0</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>e494190c-a9c1-4be6-a4b2-3793bdacfb5c</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>2f3a6da3-3579-4147-88d7-90546a84ea04</id>
      <masked>false</masked>
      <name>whatsapp</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>5838372f-50de-41fd-a67a-b15aa3955807</id>
      <masked>false</masked>
      <name>birth_date</name>
   </variables>
   <variables>
      <defaultValue>'Gambar/600x600_40kb.jpg'</defaultValue>
      <description></description>
      <id>33d80e7e-f421-4133-b943-61a17723c13d</id>
      <masked>false</masked>
      <name>photo</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>68ebf552-60bd-4762-8566-6976ed026a60</id>
      <masked>false</masked>
      <name>bio</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>3bfb845f-2d66-4028-a861-84cca911ce11</id>
      <masked>false</masked>
      <name>position</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration

import org.apache.commons.lang3.StringUtils


RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

//WS.verifyResponseStatusCode(response, 200)
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
