<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>divHargaKelas_</name>
   <tag></tag>
   <elementGuidId>a1fbaf59-06b6-4e39-8dbc-0e9dd4456bde</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2008256a-5ab4-4f46-be70-575d02529532</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-event-options</value>
      <webElementGuid>9c286532-98ea-4136-a21e-efb256fde18c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            138 Hari 20 Jam 41  Menit  52 Detik 
                            

                        
                        
                                                                                                
                                    
                                                                                
                                        Beli Tiket
                                    


                                

                                                        

                            
                        


                    
                </value>
      <webElementGuid>203aca68-172c-4f6f-b21d-315eed67f374</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4&quot;]/div[@class=&quot;wm-event-options&quot;]</value>
      <webElementGuid>533d5202-ea5f-4fc7-968a-c26bf161fb1f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      <webElementGuid>ccd4cb0c-9b09-40a0-a2c5-91dce7bfb560</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[1]/following::div[1]</value>
      <webElementGuid>a6759b13-a1fb-422d-8f4e-3d6b9bc70298</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]</value>
      <webElementGuid>7e7c37db-4c6b-4457-a66a-fe18792a7d02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            138 Hari 20 Jam 41  Menit  52 Detik 
                            

                        
                        
                                                                                                
                                    
                                                                                
                                        Beli Tiket
                                    


                                

                                                        

                            
                        


                    
                ' or . = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            138 Hari 20 Jam 41  Menit  52 Detik 
                            

                        
                        
                                                                                                
                                    
                                                                                
                                        Beli Tiket
                                    


                                

                                                        

                            
                        


                    
                ')]</value>
      <webElementGuid>e32ebe40-e7d6-4284-b143-763a1e5d889d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
