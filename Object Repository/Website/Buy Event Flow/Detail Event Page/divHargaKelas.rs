<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>divHargaKelas</name>
   <tag></tag>
   <elementGuidId>32e70b5c-8f4b-4736-91df-a310f41e17ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8df31456-564d-440e-932a-7f43e83135be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-event-options</value>
      <webElementGuid>8559293d-a224-41a3-9ef4-eb3d56b49a71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            138 Hari 20 Jam 42  Menit  0 Detik 
                            

                        
                        
                                                                                                
                                    
                                                                                
                                        Beli Tiket
                                    


                                

                                                        

                            
                        


                    
                </value>
      <webElementGuid>a036f45d-f0a3-4096-b13b-4f23b8b0fd9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4&quot;]/div[@class=&quot;wm-event-options&quot;]</value>
      <webElementGuid>722d32dc-4ca8-4345-835d-6481d4cbe39f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      <webElementGuid>eb52e99b-d858-44fe-8c95-a5114cdeb8b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[1]/following::div[1]</value>
      <webElementGuid>a95765a3-ba2a-4296-be0d-a11839d27b0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]</value>
      <webElementGuid>5edfb5a4-e5bc-4043-b0cf-71a0063873bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            138 Hari 20 Jam 42  Menit  0 Detik 
                            

                        
                        
                                                                                                
                                    
                                                                                
                                        Beli Tiket
                                    


                                

                                                        

                            
                        


                    
                ' or . = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            138 Hari 20 Jam 42  Menit  0 Detik 
                            

                        
                        
                                                                                                
                                    
                                                                                
                                        Beli Tiket
                                    


                                

                                                        

                            
                        


                    
                ')]</value>
      <webElementGuid>73facd44-6efd-4a6b-84e1-de3cb4859f74</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
