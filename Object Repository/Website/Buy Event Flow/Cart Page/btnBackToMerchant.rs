<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnBackToMerchant</name>
   <tag></tag>
   <elementGuidId>074fc937-a13d-4f78-8708-e835cb08d164</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-pay-button-part > button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='card-pay-button-part']/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Buy Event Flow/Cart Page/iframePembayaran</value>
      <webElementGuid>cd03e57d-dfd5-4800-9241-b80256743be0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
