<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify10MBSizePhoto</name>
   <tag></tag>
   <elementGuidId>90e30004-982c-42bb-a48a-f61b71611f64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='ErrorException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0f524eb3-5f83-4d21-adb1-1065eb70d431</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message</value>
      <webElementGuid>b80fbf41-6a0a-4337-bcfe-0a6cb1f305c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688730589loft-living-room-interior-design.jpg): failed to open stream: No such file or directory
</value>
      <webElementGuid>da451bfb-9b8c-425c-a2be-7483721078d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message&quot;]</value>
      <webElementGuid>c762098d-7e5c-4dc9-8822-843583a749f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ErrorException'])[1]/following::div[1]</value>
      <webElementGuid>6a1729eb-a42a-45bf-82c9-5794bc20c7cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='demo-app/'])[1]/following::div[6]</value>
      <webElementGuid>94e50f5c-f206-4359-b150-a948a3f652fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.online/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>f638fa61-61dc-4189-882e-7e5e3131cdc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>69364ae8-9183-4b3e-a156-215cd7aedd16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688730589loft-living-room-interior-design.jpg): failed to open stream: No such file or directory']/parent::*</value>
      <webElementGuid>b9862123-6351-4d09-acea-c3471a50d3c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>9545a54e-d7fe-461b-a11b-0dd7f4ae58d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688730589loft-living-room-interior-design.jpg): failed to open stream: No such file or directory
' or . = '
    fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688730589loft-living-room-interior-design.jpg): failed to open stream: No such file or directory
')]</value>
      <webElementGuid>c0c74e13-e259-49ea-994f-696b5820ec98</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
