<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verifyTelahDiEdit</name>
   <tag></tag>
   <elementGuidId>f76b3973-74d6-40ea-b250-f7c6cd363c70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.swal-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>14fdff16-3db6-4ca3-a998-d2b4c06482d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal-text</value>
      <webElementGuid>e8913bfa-c1de-43e5-a843-3df1b6251042</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> !@#$%^&amp;amp;*()_ telah di edit </value>
      <webElementGuid>62a2c9f6-c69f-4059-a0bb-f8bfb6d206ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;light light-sidebar theme-white&quot;]/div[@class=&quot;swal-overlay swal-overlay--show-modal&quot;]/div[@class=&quot;swal-modal&quot;]/div[@class=&quot;swal-text&quot;]</value>
      <webElementGuid>13bced31-97a6-4c37-9348-90af996d985e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::div[1]</value>
      <webElementGuid>0a28c343-8a72-445e-a0e5-d12bccf90191</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID'])[2]/following::div[8]</value>
      <webElementGuid>fb1c997d-f07e-403b-b77e-de9ca8630f10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::div[1]</value>
      <webElementGuid>2a63598d-739b-4742-b67b-8b41d573996d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[11]</value>
      <webElementGuid>31a51c80-d789-4c78-9217-0e1861cb83fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='!@#$%^&amp;amp;*()_ telah di edit']/parent::*</value>
      <webElementGuid>c49d3316-f5fb-411b-bb02-9785a4bb3608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]</value>
      <webElementGuid>67588a2e-2487-4b93-ac19-a96abf3ac02c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' !@#$%^&amp;amp;*()_ telah di edit ' or . = ' !@#$%^&amp;amp;*()_ telah di edit ')]</value>
      <webElementGuid>2a3dd691-4f39-40d4-b666-3aca57c8bbee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
