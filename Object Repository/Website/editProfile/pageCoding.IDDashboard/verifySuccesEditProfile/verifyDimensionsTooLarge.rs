<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verifyDimensionsTooLarge</name>
   <tag></tag>
   <elementGuidId>0ad4a17f-da2b-4974-be1e-555bd07eb9f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message.ui-exception-message-full</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='ErrorException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>39975eca-df6b-481c-8427-820477e1e155</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message ui-exception-message-full</value>
      <webElementGuid>470f7714-738b-4a42-bb3e-60f121103995</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688733286IMG_20211002_171050.jpg): failed to open stream: No such file or directory
</value>
      <webElementGuid>43b5c7b7-ad6f-4cdf-ae1b-51de280f6e45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message ui-exception-message-full&quot;]</value>
      <webElementGuid>ed3ac962-5ad7-4077-bbce-2a0ccb30925e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ErrorException'])[1]/following::div[1]</value>
      <webElementGuid>cb628abb-af32-4b86-8a39-d709f6926e78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='demo-app/'])[1]/following::div[6]</value>
      <webElementGuid>d569e683-750a-4dee-a6cf-8749c36c22e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.online/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>cde481a0-ae8b-4857-b00b-5bccbfe102a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>a094b0ae-06b4-4b44-828a-b6452b4a00cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688733286IMG_20211002_171050.jpg): failed to open stream: No such file or directory']/parent::*</value>
      <webElementGuid>1019a5a1-3446-434f-b8a4-e9ef26af0ac0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>931ff82a-7ebe-424f-ac34-c040f98e6a13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688733286IMG_20211002_171050.jpg): failed to open stream: No such file or directory
' or . = '
    fopen(/home/codingi1/public_html/codingid/demo-app/storage/app/avatar/1688733286IMG_20211002_171050.jpg): failed to open stream: No such file or directory
')]</value>
      <webElementGuid>99a705ac-cf63-4290-92be-bb21b0374db6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
