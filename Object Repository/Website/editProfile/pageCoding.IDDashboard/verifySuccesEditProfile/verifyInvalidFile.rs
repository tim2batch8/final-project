<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verifyInvalidFile</name>
   <tag></tag>
   <elementGuidId>211c915c-79e3-4b6a-abd5-7bc4f3f9bed2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f7a7b847-f0c4-4b6d-b26d-7de6359cdc8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message</value>
      <webElementGuid>dd207601-09af-4d35-aea1-5356696dfe60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
</value>
      <webElementGuid>d743b533-43be-46c5-b06e-caa2f1bd5998</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message&quot;]</value>
      <webElementGuid>9f940231-9846-4ae9-8a49-914c472087ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      <webElementGuid>b1f7a876-580b-4905-b613-3f08e1cce891</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exception\'])[1]/following::div[1]</value>
      <webElementGuid>05c273a3-73d9-494d-b648-32bf89d03a95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.online/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>26f76e20-1560-448c-875e-18eed3cad370</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>17a2b2f5-a40d-4dc6-8ebe-b05ed282a317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.']/parent::*</value>
      <webElementGuid>a89c9a38-bbc1-4060-b589-78df76436f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>1b2ceb91-b43f-42af-a118-ae9be136cc94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
' or . = '
    Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
')]</value>
      <webElementGuid>c56e2c3f-7270-4a0d-b2c6-178d81bde55e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
