<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verifyFullnameDefect</name>
   <tag></tag>
   <elementGuidId>4701f7dd-e54c-49cd-a1eb-87f6b0f2631d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div/span/strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>41a19373-5753-4c47-a5af-9d51c64d9cf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The name field is required.</value>
      <webElementGuid>2cc16364-d219-48c9-8d25-dd9ac25ef3ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-4&quot;]/div[@class=&quot;form-group&quot;]/span[@class=&quot;invalid-feedback&quot;]/strong[1]</value>
      <webElementGuid>dffa4c70-a24a-47f2-989f-490b2d4bdeca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div/span/strong</value>
      <webElementGuid>49e932cf-2587-4ede-846d-f6ff22500c3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullname'])[1]/following::strong[1]</value>
      <webElementGuid>bbc86d41-4fa7-46e9-b760-f5b89a5d313f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::strong[1]</value>
      <webElementGuid>ca513b45-2e32-40d8-9c69-905c3c9ad845</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/preceding::strong[1]</value>
      <webElementGuid>2efea5a8-d104-4636-91c2-01df41d84712</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone'])[1]/preceding::strong[1]</value>
      <webElementGuid>b030c002-9e81-4d88-ae17-92f525693038</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The name field is required.']/parent::*</value>
      <webElementGuid>4c39f549-036f-4717-b3b4-2ed6f9f9c457</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>07ba2d77-d702-4a0d-ba96-d203502c5694</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'The name field is required.' or . = 'The name field is required.')]</value>
      <webElementGuid>8d355d67-4317-4085-b12d-e1dfb3e7d70c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
