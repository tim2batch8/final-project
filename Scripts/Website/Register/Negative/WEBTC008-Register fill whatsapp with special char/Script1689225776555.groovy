	import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.urlWeb)

WebUI.click(findTestObject('Website/Register/buttonBuatAkun'))


WebUI.setText(findTestObject('Website/Register/inputNama'), nama)

WebUI.setText(findTestObject('Website/Register/inputTanggalLahir'), tglLahir, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Website/Register/inputEmail'), email)

WebUI.setText(findTestObject('Website/Register/inputWhatsapp'), whatsapp)

WebUI.setText(findTestObject('Website/Register/inputPassword'), password)

WebUI.setText(findTestObject('Website/Register/inputKonfirmasiPassword'), confirmPass)

WebUI.click(findTestObject('Website/Register/inputCheckbox'))

WebUI.takeScreenshot('Test/TC09.png')

WebUI.click(findTestObject('Website/Register/buttonDaftar'))

WebUI.takeScreenshot('Test/TC09-2.png')

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Website/Register/inputWhatsapp'), 'validationMessage'), 
    'Please enter a number.')

