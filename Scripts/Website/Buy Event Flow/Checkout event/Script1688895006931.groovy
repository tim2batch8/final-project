import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

btnBuatAkun = findTestObject('Object Repository/Website/Register/buttonBuatAkun')

WebUI.openBrowser(GlobalVariable.urlWeb)

if (WebUI.verifyElementPresent(btnBuatAkun, 0)) {
    WebUI.callTestCase(findTestCase('Website/baseLogin/baseLogin-Galang'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('Website/Buy Event Flow/Home Page/menuEvent'))

    elementText = WebUI.getText(findTestObject('Website/Buy Event Flow/Event Page/cardDay4', [('cardName') : cardName]))

    WebUI.click(findTestObject('Website/Buy Event Flow/Event Page/cardDay4', [('cardName') : cardName]))

    WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Detail Event Page/spanDetailEvent'), 
        0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Detail Event Page/divHargaKelas_'), 
        0)

    if (elementText == 'Day 2: Data Wrangling with Python') {
        WebUI.verifyElementText(findTestObject('Object Repository/Website/Buy Event Flow/Detail Event Page/textPendaftaranTutup'), 
            'PENDAFTARAN DITUTUP' // Blok kode yang akan dieksekusi jika objek dan teks cocok
            //        WebUI.verifyElementNotChecked(findTestObject('Website/Buy Event Flow/Cart Page/checkboxEvent'), 0)
            )

        WebUI.comment('Negative case closed event')
    } else {
        WebUI.click(findTestObject('Website/Buy Event Flow/Detail Event Page/buttonBeliTiketLogin'))

        WebUI.click(findTestObject('Website/Buy Event Flow/Detail Event Page/buttonLihatPembelianSaya'))

        WebUI.verifyElementPresent(findTestObject('Website/Buy Event Flow/Cart Page/h3TextEvent'), 0)

        eventName = WebUI.getText(findTestObject('Website/Buy Event Flow/Cart Page/h3TextEvent'))

        WebUI.verifyElementText(findTestObject('Website/Buy Event Flow/Cart Page/h3TextEvent'), eventName)

        WebUI.click(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/checkboxEvent'))

        WebUI.comment('Negative Case Not Check Checkout Button')

        try {
            WebUI.verifyElementNotChecked(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/checkboxEvent'), 
                0)

            WebUI.click(findTestObject('Website/Buy Event Flow/Cart Page/btnCheckout'))

            WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/infoNoEventCheckout'), 
                0)

            WebUI.click(findTestObject('Website/Buy Event Flow/Cart Page/btnOKNoEventCheckout'))

            WebUI.comment('Referral Code')

            WebUI.click(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/checkboxEvent'))

            WebUI.setText(findTestObject('Website/Buy Event Flow/Cart Page/formReferral'), referral)

            WebUI.click(findTestObject('Website/Buy Event Flow/Cart Page/btnApplyReferral'))

            message = WebUI.getText(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/successMessageReferral'))

            if (message == 'success_message') {
                WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/successMessageReferral'), 
                    0)
				
				
				WebUI.click(findTestObject('Website/Buy Event Flow/Cart Page/btnCheckout'))
            } else {
                WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Cart Page/errorMessageReferral'), 
                    0)
            }
        }
        catch (Exception e) {
            WebUI.click(findTestObject('Website/Buy Event Flow/Cart Page/btnCheckout'))
        } 
    }
} else {
    WebUI.click(findTestObject('Website/Buy Event Flow/Home Page/menuEvent'))

    WebUI.click(findTestObject('Object Repository/Website/Buy Event Flow/Event Page/cardDay4'))

    WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Detail Event Page/spanDetailEvent'), 
        0)

    WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Buy Event Flow/Detail Event Page/divHargaKelas_'), 
        0)

    WebUI.click(findTestObject('Object Repository/Website/Buy Event Flow/Detail Event Page/buttonBeliTiketnotLogin'))

    WebUI.click(findTestObject('Website/Buy Event Flow/Detail Event Page/buttonLihatPembelianSaya'))

    WebUI.verifyElementPresent(findTestObject('Website/Buy Event Flow/Cart Page/h3TextEvent'), 0)

    eventName = WebUI.getText(findTestObject('Website/Buy Event Flow/Cart Page/h3TextEvent'))

    WebUI.verifyElementText(findTestObject('Website/Buy Event Flow/Cart Page/h3TextEvent'), eventName)
}

