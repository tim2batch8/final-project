import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.callTestCase(findTestCase('Website/baseLogin/baseLogin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Website/loginPage/verifyElementLogin/userLogo'))

WebUI.click(findTestObject('Website/editProfile/pageBeranda/btnMyAccount'))

WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/btnMenuProfil'))

WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/btnEditProfile'))

WebUI.uploadFile(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpPhoto'), RunConfiguration.getProjectDir() + '/Gambar/600x600_40kb.jpg')

WebUI.setText(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpFullname'), 'Timdua Email')

WebUI.setText(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpPhone'), '087812121313')

WebUI.setText(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpBirthDay'), '19-Jan-1998')

WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpEmail'))

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC13-1.png')

WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/btnSaveChanges'))

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC13-2.png')

WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/FullnameValidasi/btnOkBerhasilUpdateProfil'))

WebUI.closeBrowser()

