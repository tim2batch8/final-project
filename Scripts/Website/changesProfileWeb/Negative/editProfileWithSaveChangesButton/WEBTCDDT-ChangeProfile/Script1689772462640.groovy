import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Website/baseLogin/baseLogin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Website/loginPage/verifyElementLogin/userLogo'))

WebUI.click(findTestObject('Website/editProfile/pageBeranda/btnMyAccount'))

WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/btnMenuProfil'))

for (int rowData = 1; rowData <= findTestData('Data Files/Website/changeProfile').getRowNumbers(); rowData++) {
    fullname = findTestData('Data Files/Website/changeProfile').getValue(1, rowData)

    phone = findTestData('Data Files/Website/changeProfile').getValue(2, rowData)

    birthday = findTestData('Data Files/Website/changeProfile').getValue(3, rowData)

    WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/btnEditProfile'))

    WebUI.setText(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpFullname'), fullname)

    WebUI.setText(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpPhone'), phone)

    WebUI.setText(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpBirthDay'), birthday)

    WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/inpEmail'))

    WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/btnSaveChanges'))

    if (rowData == 1) {
        WebUI.verifyElementVisible(findTestObject('Website/editProfile/pageCoding.IDDashboard/FullnameValidasi/verifyFullnameBlank'))

        WebUI.verifyElementVisible(findTestObject('Website/editProfile/pageCoding.IDDashboard/PhoneField/verifyPhoneFieldBlank'))

        WebUI.verifyNotEqual(birthday, findTestObject('Website/editProfile/pageCoding.IDDashboard/inpBirthDay'))

        WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/verifySuccesEditProfile/btnCancel'))
    } else if (rowData == 2) {
        WebUI.verifyElementVisible(findTestObject('Website/editProfile/pageCoding.IDDashboard/PhoneField/verifyPhoneDigitUnder10'))

        WebUI.verifyElementVisible(findTestObject('Website/editProfile/pageCoding.IDDashboard/FullnameValidasi/verifyFullnameDefect'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.verifyNotEqual(birthday, findTestObject('Website/editProfile/pageCoding.IDDashboard/inpBirthDay'))

        WebUI.comment('Negative test case')

        WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/verifySuccesEditProfile/btnCancel'))
    } else {
        WebUI.verifyElementVisible(findTestObject('Website/editProfile/pageCoding.IDDashboard/FullnameValidasi/btnOkBerhasilUpdateProfil'))

        WebUI.click(findTestObject('Website/editProfile/pageCoding.IDDashboard/FullnameValidasi/btnOkBerhasilUpdateProfil'))
    }
}

WebUI.closeBrowser()

