import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.WebsiteUrl)

WebUI.setText(findTestObject('Website/loginPage/validElementLogin/inputEmail'), GlobalVariable.emailGetnada)

WebUI.sendKeys(findTestObject('Website/loginPage/validElementLogin/inputKataSandi'), GlobalVariable.passwordGetnada)

WebUI.click(findTestObject('Website/loginPage/validElementLogin/buttonLogin'))

WebUI.verifyElementVisible(findTestObject('Website/loginPage/verifyElementLogin/userLogo'))

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC02-1.png')

WebUI.click(findTestObject('Website/loginPage/TC02/DasboardMenu/iconKontakMenu'))

WebUI.click(findTestObject('Website/loginPage/TC02/DasboardMenu/btnMyAccountMenu'))

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC02-2.png')

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyBillingAccount'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyCoding.ID'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyDashboard'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyInvoice'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyMyCourse'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyMyEvents'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyMyPoints'))

WebUI.verifyElementClickable(findTestObject('Website/loginPage/TC02/pageCoding.IDDashboard/verifyProfil'))

WebUI.delay(3)

WebUI.closeBrowser()

