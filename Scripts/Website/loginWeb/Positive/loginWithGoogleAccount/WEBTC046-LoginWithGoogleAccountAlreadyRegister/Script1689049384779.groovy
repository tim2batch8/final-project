import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.WebsiteUrl)

WebUI.click(findTestObject('Website/loginPage/signInWithGoogleAccount/pageLogin/btnSignInWithGoogle'))

WebUI.sendKeys(findTestObject('Website/loginPage/signInWithGoogleAccount/PageSignInGoogle/inputEmailGoogle'), 'tim2batch8@gmail.com')

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC10-1.png')

WebUI.click(findTestObject('Website/loginPage/signInWithGoogleAccount/PageSignInGoogle/btnNextPageEmail'))

WebUI.sendKeys(findTestObject('Website/loginPage/signInWithGoogleAccount/PageSignInGoogle/inputPassword'), 'P@ssw0rd123!')

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC10-2.png')

WebUI.click(findTestObject('Website/loginPage/signInWithGoogleAccount/PageSignInGoogle/btnNextPagePassword'))

WebUI.verifyElementVisible(findTestObject('Website/loginPage/verifyElementLogin/userLogo'))

WebUI.takeScreenshot('Screenshots/Website/loginWeb/TC10-3.png')

WebUI.closeBrowser()

