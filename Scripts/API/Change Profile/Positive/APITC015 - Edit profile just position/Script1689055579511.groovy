import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.webservice.verification.WSResponseManager as WSResponseManager


WS.sendRequestAndVerify(findTestObject('API/Login', [('token') : '', ('email') : GlobalVariable.emailGetnada, ('password') : GlobalVariable.passwordGetnada]))

ResponseObject respLogin = WSResponseManager.getInstance().getCurrentResponse()
GlobalVariable.token = WS.getElementPropertyValue(respLogin, 'success.token')

WS.sendRequestAndVerify(findTestObject('API/Change Profile', [('name') : '', ('whatsapp') : '', ('birth_date') : '', ('photo') : ''
            , ('bio') : '', ('position') : position]))

ResponseObject respChangeProfile = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(respChangeProfile, 200)

WS.sendRequestAndVerify(findTestObject('API/Login', [('token') : '', ('email') : GlobalVariable.emailGetnada, ('password') : GlobalVariable.passwordGetnada]))

ResponseObject respLogin2 = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyEqual(WS.getElementPropertyValue(respLogin2, 'success.position'), position)

WS.verifyResponseStatusCode(respLogin, 200)

