import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
//Mobile.verifyElementVisible(findTestObject('Object Repositoimport static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Gambar/ApkMobile/DemoAppV2FinalProject.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile/changeProfileMobile/baseLogin/btnLoginHere'), 0)

Mobile.sendKeys(findTestObject('Object Repository/Mobile/changeProfileMobile/baseLogin/inpEmail'), 'aloalaologetnada.com')

Mobile.sendKeys(findTestObject('Object Repository/Mobile/changeProfileMobile/baseLogin/inpPassword'), 'P@ssw0rd')

Mobile.tap(findTestObject('Object Repository/Mobile/changeProfileMobile/baseLogin/btnLogin'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Mobile/Login/notifInvalid'), 0)

