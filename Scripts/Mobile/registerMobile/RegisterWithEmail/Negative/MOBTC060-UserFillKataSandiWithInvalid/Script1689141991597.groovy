import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Gambar/ApkMobile/DemoAppV2FinalProject.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile/changeProfileMobile/baseLogin/btnLoginHere'), 0)

Mobile.tap(findTestObject('Mobile/registerMobile/SignUpWithGoogle/btnRegisterNow1'), 0)

Mobile.sendKeys(findTestObject('Mobile/registerMobile/inpRegister2/inpKataSandiField5'), 'invalidpassword', FailureHandling.STOP_ON_FAILURE)

Mobile.takeScreenshot('Screenshots/Mobile/Register/MOBTC060-1.png')

Mobile.verifyElementVisible(findTestObject('Mobile/registerMobile/verifyObjectRegister/verifyKataSandiAlphanumeric(a-Z,0-9)Min8Chrctr'), 
    0, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

