<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>64b725ec-4df5-44fc-a32b-eef90c4bfb14</testSuiteGuid>
   <testCaseLink>
      <guid>37f9d166-5ace-4d60-a2cc-fe89826ce8cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC032-RegisterUserFillAllWithValid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b8ad102d-6150-4b39-a2a7-8f4ca337b3d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC039-UserInputDateOfBirthWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0c8efb3d-34df-4abb-9bfa-07b9b7e5cb9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC040-UserFillNamaWithValid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c3a13ab6-33fe-4edb-bd69-62ed121bda3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC041-UserFillNamaWithNumber</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>269fd354-2559-4ff2-a8fe-491d39d94d9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC042-UserFillNamaWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7759320a-a0ea-443a-be1a-00774bafe62c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC043-UserFillNamaWithInvalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9b4f2cf3-5369-460b-a831-0b1a3d302a14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC044-UserFillEmailWithValid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6b69e0aa-a129-4ef6-b74b-01b2ce7ef574</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC045-UserFillEmailWithInvalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6d3de673-4e34-490a-8c41-c599e5e7a727</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC046-UserFillEmailWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>81e023e8-d759-4e2d-b13c-b0008e35a0d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC047-UserFillEmailWithRegisteredEmail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ea6b0be7-22eb-4d2d-97bd-74ba89a5c784</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC049-UserFillWhatsappWithValid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>237bcb1d-068d-461e-84dc-45fce884f3d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC051-UserFillWhatsappWith8Digits</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ec13957a-721e-49ac-bbf3-67c5789e75bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC052-UserFillWhatsappWith9Digits</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>25d44f5a-25e7-42ff-b0cd-9a83ad2b1ffc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC053-UserFillWhatsappWith13Digits</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d2965252-b45b-4f24-bbc3-debcf3d754f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC054-UserFillWhatsappWith14Digits</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cd12e2c1-c61a-4f65-8865-338b08fb38ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC055-UserFillWhatsappWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e9ef5915-0a92-483f-b273-2a2f2d27d24f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC056-UserFillWhatsappWithOtherThanNumber</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9ddc4534-dd98-4b92-91ab-a8ad41fbaf8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC057-UserFillKataSandiWith8CharacterAndValid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>81ae073d-1b92-4d94-9d17-d0dcca111cd5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC058-UserFillKataSandiWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>005c635b-fd32-4f3d-9883-ed8dea28063c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC059-UserFillKataSandiWithUnder8Cha</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c687d994-1e86-4f5e-b08e-4f89debf66ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC060-UserFillKataSandiWithInvalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c7904102-b78a-4e75-bbde-bbf9e32eaded</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC061-UserFillKataSandiWithThaiChacAndMixAlpaNu</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>028f5aba-ead2-4531-a240-ecdd722d53c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Positive/MOBTC063-UserFillKonfirmasiKSWithCorrect</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e40102de-08d0-47b8-9906-23f6ffb1587d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/registerMobile/RegisterWithEmail/Negative/MOBTC064-UserFillKonfirmasiKSWithDifferent</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
