<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LoginWeb</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4363a338-3e6d-4c75-946a-383118ca3f12</testSuiteGuid>
   <testCaseLink>
      <guid>b35cb2b6-d3f3-4ec9-bee9-ecd83b30795d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Positive/loginWithLoginButton/WEBTC037-LoginWithEmailAndPasswordValidInput</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3aea338f-766e-4498-96ae-3a0c03fe1833</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Positive/loginWithLoginButton/WEBTC038-AccessDashboardFeatureAndUserAlreadyLogin</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fb1c1f84-a50f-4576-95af-274d67b6feac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC039-LoginWithWrongEmail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e767d94e-9ac9-48a5-afc5-5d62d0caa6a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC040-LoginWithWrongPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f2744c93-4c9c-4667-b9fb-fabc96c9e3d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC041-LoginWithInvalidEmail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1022708f-8f53-40de-9f0e-980722566b67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC042-LoginWithBlankEmail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ddea610b-4b97-45a0-9683-a4b1afdef7b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC043-LoginWithBlankPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2d0ea8c3-1e2d-4952-989a-9da36fbaa88d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC044-LoginWithBlankEmailAndBlankPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cc1a27a3-b520-4961-be85-76c08f08f032</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithLoginButton/WEBTC045-LoginWithInvalidEmailAndBlankPassword</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4599f535-c6c9-4752-b56d-62e13ba815e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Positive/loginWithGoogleAccount/WEBTC046-LoginWithGoogleAccountAlreadyRegister</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4f46cf00-00fd-4f9c-a905-e1226b4e5fb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Negative/loginWithGoogleAccount/WEBTC047-LoginWithGoogleAccountUnregistered</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ab5b1222-8292-468b-9d4a-a6d1b988e18e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/loginWeb/Positive/loginWithPressingEnterOnKeyboard/WEBTC048-LoginWithEmailAndPasswordValidInput</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
