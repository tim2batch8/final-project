<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ChangeProfileWeb</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e32a2177-281f-4a27-a54e-5af1141ce0aa</testSuiteGuid>
   <testCaseLink>
      <guid>1ed6eaa2-6469-43a5-b45d-63d35fa55f93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithSaveChangesButton/WEBTC049-EditProfileByFillingAllFieldsWithValidInput</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>49b2460e-083e-43ce-94cd-b4148420a9d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithSaveChangesButton/WEBTC050-EditEmailField</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5ad6d902-2934-4468-a434-e99df59e7e96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithSaveChangesButton/WEBTC051-ChangePhotoWithValidFile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>087786f6-bf22-4b18-a7ab-065b1427b18f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC052-ChangePhotoWithInvalidFile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c2253dbd-2bbf-4e1e-9d82-13a153201a2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC053-ChangePhotoWithSizeLargerThan2MB</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>82f4269d-0197-4fc1-8258-212c2dfbf5ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC054-ChangePhotoWithDimensionsGreaterThan2000</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>79c31fc0-5f63-4965-9b9d-5a1860f14282</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC055-EditPhoneFieldWithUnder10Digit</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>14150425-680d-480b-998a-6c0b57085550</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC056-EditPhoneFieldLongerThan12Digits</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f45394de-c5e8-4747-b554-fc500ed0776b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC057-EditPhoneFieldWithAlphabet</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fabdd6b7-da24-434f-9b98-43d256c731e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC058-EditPhoneFieldWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0158c25a-c7f1-4459-8d8f-c0c34dca3b16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithSaveChangesButton/WEBTC059-EditBirthdayFieldWithAge6YearPrecisely</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9a450e26-1bf3-47d4-b501-9bf23fb3d39f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithSaveChangesButton/WEBTC060-EditBirthdayFieldWithAge6Year5Mont29Day</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b64c4306-271d-40a9-a6f6-c2946ae2b13f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithSaveChangesButton/WEBTC061-EditBirthdayFieldWithAge6Year6Mont7Day</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6336d248-8594-426d-8dac-7c2aa818b67b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC062-EditBirthdayFieldWithAgeUnder6Year</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4ed66dcc-0da4-40ee-8df5-d9fddd47d0e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC063-EditBirthdayFieldWithAge100Precisely</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>dbbeeac6-ed3b-4b10-8bf3-24b805002e5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC064-EditBirthdayFieldWithOver100Year</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e5d5929d-a95b-4848-83fb-d6c621af279d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC065-EditFullnameFieldWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>358a2867-ac4f-4579-a266-65c18d02a91c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC066-EditBirthdayFieldWithBlank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8c52a736-7682-4d00-8264-d40564e6851a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC067-EditFullnameFieldWithNumber</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>507270ed-63f8-4a91-9756-e82c06c828fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC068-EditFullnameFieldWithThailandLetter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>97825c98-b8a8-4c4a-a9e7-a76d64aaf390</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Negative/editProfileWithSaveChangesButton/WEBTC070-EditFullnameFieldWithMixCharacter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4d4b6c47-bdcc-468e-ac45-6492a3a2c60c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/changesProfileWeb/Positive/editProfileWithPressingEnterOnKeyboard/WEBTC71-EditProfileByFillingAllFieldsWithValidInput</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
